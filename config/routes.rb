Rails.application.routes.draw do
  get 'menu/index'
  root to:  "menu#index"
  resources :reservations
  resources :books
  resources :authors
  resources :clients
  resources :librarians
  resources :librarian
  devise_for :librarian, path: 'euquelute'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
