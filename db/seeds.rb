# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
print "Gerando nomes de Autores(Authors) ... "
    Author.create!([{name:"Monteiro Lobato"},
                {name:"José de Alencar"},
                {name:"Carlos Drummond de Andrade"},
                {name:"Machado de Assis"},
                {name:"João Cabral de Melo Neto"}
                ])
    puts "[OK]"


print "Gerando Clientes(Clients) ... "
    Client.create!([{name:"Larissa Manoela"},
                {name:"Sidoka"},
                {name:"Mundinho Prior"},
                {name:"Babu"},
                {name:"Prizelly"}
                ])
puts "[OK]"


print "Gerando nomes dos bibliotecários(Librarians) ... "
    Librarian.create!([
                       {name: "Selena Gomez", 
                        email:"selenagomez@gmail.com",
                        password:"123456",
                        password_confirmation:"123456"}])
    puts "[OK]"
